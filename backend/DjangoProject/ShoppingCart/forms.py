from django import forms
from Store.models import Product
from .models import Customer


class AddProductForm(forms.Form):
    quantity = forms.TypedChoiceField(choices=((x, x) for x in range(1, 11)), coerce=int)
    update = forms.BooleanField(required=False, initial=False, widget=forms.HiddenInput)


class OrderCreatingForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ('name', 'surname', 'email', 'city', 'address', 'postal_code')
        labels = {'postal_code': 'city postal code'}
        help_texts = {'postal_code': 'Enter the postal code of your city', 'address': 'Enter your address',
                      'city': 'Enter your city'}
