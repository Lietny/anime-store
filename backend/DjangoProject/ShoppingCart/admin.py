from django.contrib import admin
from .models import Order, Customer


class OrderInline(admin.TabularInline):
    model = Order
    raw_id_fields = ['product']


@admin.register(Customer)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['name', 'surname', 'email', 'city', 'address', 'postal_code',
                    'order_created', 'order_updated', 'payment']
    list_editable = ['postal_code']
    list_filter = ['order_created', 'order_updated', 'payment']
    inlines = [OrderInline]
