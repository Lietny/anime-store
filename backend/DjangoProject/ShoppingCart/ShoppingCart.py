from decimal import Decimal
from django.conf import settings
from Store.models import Product


class Cart:
    def __init__(self, request):
        """
        initialize shopping cart, add to user session CART_SESSION_ID
        :param request: type => HttpRequest(). user request objects
        """
        self.session = request.session  # session dict
        shopping_cart = self.session.get(settings.CART_SESSION_ID)  # get CART_SESSION_ID key in session
        if not shopping_cart:  # if key not found
            shopping_cart = self.session[settings.CART_SESSION_ID] = {}  # create CART_SESSION_ID. Set ShoppingCart: {}
        self.shopping_cart = shopping_cart  # initialize session struct

    def add_product(self, product, product_quantity=1, update_quantity=False):
        """
        add product to shopping cart, selection quantity, update quantity
        :param update_quantity: Boolean variable for update quantity
        :param product: ORM Model
        :param product_quantity: quantity of goods
        """
        product_id = str(product.pk)  # get product id in string type
        if product_id not in self.shopping_cart.keys():  # if product not in shopping cart
            self.shopping_cart[product_id] = {'quantity': 0, 'price': str(product.price)}  # init cart with product
        if update_quantity:
            self.shopping_cart[product_id]['quantity'] = product_quantity
        else:
            self.shopping_cart[product_id]['quantity'] += product_quantity
        self.save()  # save shopping cart state

    def remove_product(self, product):
        """
        remove product from shopping cart
        :param product: ORM Model
        """
        product_id = str(product.id)  # get product id in string type
        if product_id in self.shopping_cart.keys():  # if product in shopping cart
            self.shopping_cart.pop(product_id)  # delete product from shopping cart
            self.save()  # save shopping cart state

    def save(self):
        """
        rewriting session CART_SESSION_ID
        """
        self.session[settings.CART_SESSION_ID] = self.shopping_cart
        self.session.modified = True

    def __iter__(self):
        """
        showing products in the shopping cart
        struct: CART_SESSION_ID: product_id: {'product': ***, 'price': ***, 'full_price': ***, 'quantity': ***}
        """
        products_id = self.shopping_cart.keys()  # get products in cart
        products = Product.objects.filter(pk__in=products_id)  # filter products by products_id
        for product_val in products:
            self.shopping_cart[str(product_val.pk)]['product'] = product_val  # product_id : {,,'product': prod_val}

        for items in self.shopping_cart.values():
            items['price'] = Decimal(items['price'])
            items['full_price'] = items['price'] * items['quantity']  # product_id : {,,'full_price': price*quantit}
            yield items

    def full_price(self):
        """
        :return: total cost of production
        """
        return sum(Decimal(item['price']) * item['quantity'] for item in self.shopping_cart.values())

    def __len__(self):
        return sum(item['quantity'] for item in self.shopping_cart.values())

    def clear_cart_session(self):
        """
        clear session CART_SESSION_ID
        """
        del self.session[settings.CART_SESSION_ID]
        self.session.modified = True

