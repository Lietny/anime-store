from django.db import models
from Store.models import Product
from phone_field import PhoneField


class Customer(models.Model):
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=70)
    email = models.EmailField()
    city = models.CharField(max_length=150)
    address = models.CharField(max_length=300)
    postal_code = models.CharField(max_length=50)
    order_created = models.DateTimeField(auto_now_add=True)
    order_updated = models.DateTimeField(auto_now=True)
    payment = models.BooleanField(default=False)

    class Meta:
        ordering = ['order_created']
        verbose_name = 'Customer'
        verbose_name_plural = 'Customers'

    def __str__(self):
        return f'{self.name} {self.surname}'

    @staticmethod
    def order_access(order_id):
        custom_order = Customer.objects.get(pk=order_id)
        days_order_acceptance = custom_order.order_created.day
        if days_order_acceptance > days_order_acceptance + 5:
            custom_order.customer_items.access = False


class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE,
                                 related_name='customer', related_query_name='customer_items')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='product',
                                related_query_name='product_items')
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveSmallIntegerField(default=1)
    access = models.BooleanField(default=True)

    def access_validator(self):
        pass

    class Meta:
        ordering = ['quantity']
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

    def __str__(self):
        return self.product.name

