from django.contrib import admin
from .models import Product, Category
from mptt.admin import MPTTModelAdmin


# Register your models here.

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'price', 'stock',
                    'available', 'created_date', 'updated_date')
    prepopulated_fields = {'slug': ('name', )}
    list_editable = ('price', 'stock', 'available')
    list_filter = ('available', 'created_date', 'updated_date')
    list_display_links = ('name', )
    search_fields = ('name', 'description')


# Admin site tree
@admin.register(Category)
class CustomMPTTCategoryAdmin(MPTTModelAdmin, admin.ModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name', )}
    list_display_links = ('name', )
    search_fields = ('name', )
    # pixels per level for CustomMPTTCategoryAdmin only. default 10px
    mptt_level_indent = 20


