from django.db import models
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey


# Create your models here.


class Publisher(models.Model):
    pass


class Category(MPTTModel, models.Model):
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    name = models.CharField(max_length=100, db_index=True, unique=True)
    slug = models.SlugField(max_length=100, db_index=True)

    class Meta:
        ordering = ['name']
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    class MPTTMeta:
        order_insertion_by = ['name']

    def get_absolute_url(self):
        return reverse('Store:products_with_category_slug', kwargs={'category_slug': self.slug})

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=150, db_index=True)
    description = models.TextField(max_length=4000)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.ImageField(upload_to='ProductImage', blank=True)
    stock = models.PositiveIntegerField()
    available = models.BooleanField()
    slug = models.SlugField(max_length=100, db_index=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    category = models.ManyToManyField(Category, related_query_name='entry')

    class Meta:
        index_together = (('id', 'slug'),)
        ordering = ['name', 'price']
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def get_absolute_url(self):
        return reverse('Store:ProductDetail',
                       kwargs={'product_id': self.pk, 'product_slug': self.slug})

    def __str__(self):
        return self.name
