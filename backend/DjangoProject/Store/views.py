from django.shortcuts import render, HttpResponse, get_object_or_404
from .models import Product, Category
from ShoppingCart.forms import AddProductForm
from django.views.generic import DetailView, ListView
from .handlers import pagination_handler


def ProductList(request, category_slug=None):
    """
    used pagination
    :param request: type => HttpRequest(). user request objects
    :param category_slug: type => category url-slug
    :return: render: type => HttpResponse object, template = ProductList.html, context = context_dict
            if Slug: context_dict = specific categories and related products.
    """
    categories = Category.objects.all()
    products = Product.objects.filter(available=True)
    product_limit = pagination_handler(request, products, 5)
    context_dict = {'categories': categories, 'product_limit': product_limit}
    if category_slug:
        category = get_object_or_404(Category, slug=category_slug)
        filtered_products = products.filter(category=category)
        product_limit = pagination_handler(request, filtered_products, 5)
        context_dict.update({'product_limit': product_limit, 'category': category})
    return render(request, 'Store/ProductList.html', context=context_dict)


# class ProductDetail(DetailView):
#     template_name = 'Store/ProductDetail.html'
#     model = [Product, Category]
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         product_detail = get_object_or_404(Product, pk=kwargs['product_id'],
#                                                       slug=kwargs['product_slug'], available=True)
#         context['product_detail'] = product_detail.objects.all()
#         context['nested'] = product_detail.category.all()
#         context['add_product_to_cart'] = AddProductForm()
#         return context


def ProductDetail(request, product_id, product_slug):
    """
    receipt specific product by id and product_slug, formation of nested categories.
    Obtaining a form for choosing the quantity of goods.
    :param request: type => HttpRequest(). user request objects
    :param product_id: type => int. product id
    :param product_slug: type => product url-slug
    :return: render: type => HttpResponse object, template = ProductDetail.html, context = context_dict
    """
    product_detail = get_object_or_404(Product, pk=product_id, slug=product_slug, available=True)
    nested_category = product_detail.category.all()
    add_product_to_cart = AddProductForm()
    context_dict = {'product_detail': product_detail,
                    'nested': nested_category,
                    'add_product_to_cart': add_product_to_cart}
    return render(request, 'Store/ProductDetail.html', context=context_dict)


