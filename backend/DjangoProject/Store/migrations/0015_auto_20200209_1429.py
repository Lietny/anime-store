# Generated by Django 3.0.3 on 2020-02-09 11:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Store', '0014_auto_20200209_0957'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['name'], 'verbose_name': 'Category', 'verbose_name_plural': 'Categories'},
        ),
    ]
